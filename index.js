$(document).ready(function () {
    $("#close").click(function () {
        $(".sidenav").animate({
            width: "0px"
        });
        $(".main").animate({
            margin: "auto auto auto 0px"
        });
    });
    $("#expand").click(function () {
        $(".sidenav").animate({
            width: "260px"
        });
        $(".main").animate({
            margin: "auto auto auto 260px"
        });
    });
    $("#btn").click(function () {
        $(".sidenav").animate({
            width: "160px"
        });
        $(".main").animate({
            margin: "auto auto auto 160px"
        });
    });   
}); 
